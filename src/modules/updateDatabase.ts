/* eslint-disable no-console */
import { UnionsClient } from 'src/clients/unionsClient';
import { getCategories, getRegions, getSindicatosByRegion } from './getSindicatos';
import { RegionType, UnionsType } from './types';

const unionsClient = new UnionsClient();

const addUnionList = async (): Promise<void> => {
	const regions: RegionType[] = getRegions();

	const promises: Promise<void>[] = [];

	regions.forEach(({ slug }) => {
		const regionNumber = slug.split('_')[0];

		const unions: UnionsType[] = getSindicatosByRegion(regionNumber);

		console.log(`Adding ${unions.length} unions to DB of region ${slug}`);
		unions.map((union) => promises.push(unionsClient.addUnion(union)));
	});

	await Promise.all(promises).then(() => {
		console.log('done');
	});
};

const addRegions = async (): Promise<void> => {
	const regions: RegionType[] = getRegions();
	const promises: Promise<void>[] = [];

	regions.forEach((region) => promises.push(unionsClient.addRegion(region)));

	await Promise.all(promises).then(() => {
		console.log('done');
	});
};

const addUnionCategories = async (): Promise<void> => {
	const categories = getCategories();
	const promises: Promise<void>[] = [];

	categories.forEach((category) => promises.push(unionsClient.addCategory(category)));

	await Promise.all(promises).then(() => {
		console.log('done');
	});
};

const getUnionList = async (): Promise<UnionsType[]> => {
	const unionList: UnionsType[] = await unionsClient.getUnionList();

	console.log('unionList size: ', unionList.length);

	return unionList;
};

// eslint-disable-next-line @typescript-eslint/no-floating-promises
// addUnionList();

// eslint-disable-next-line @typescript-eslint/no-floating-promises
// getUnionList();

// eslint-disable-next-line @typescript-eslint/no-floating-promises
// addRegions();

// eslint-disable-next-line @typescript-eslint/no-floating-promises
addUnionCategories();

export { addUnionList, addRegions, getUnionList };
