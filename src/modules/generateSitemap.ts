import fs from 'fs';

import { configureSitemap } from '@sergeymyssak/nextjs-sitemap';

const BASE_URL = 'https://sindicatos.now.sh';

const getDynamicPaths = (path: string, paths: string[]) => {
	if (path) return paths.map((item) => `/${path}/${item}`);
	return paths.map((item) => `/${item}`);
};

export const buildSitemap = async (path: string, paths: string[]): Promise<void> => {
	console.log(`start buildSitemap ${path}`);

	createDirectory(path);

	const Sitemap = configureSitemap({
		baseUrl: BASE_URL,
		include: getDynamicPaths(path, paths),
		exclude: ['/*'],
		excludeIndex: true,
		pagesConfig: {
			'/*': {
				priority: '0.5',
				changefreq: 'monthly',
			},
		},
		isTrailingSlashRequired: false,
		targetDirectory: `${process.cwd()}/public/${path}`,
		pagesDirectory: `${process.cwd()}/pages`,
	});

	await Sitemap.generateSitemap();
	console.log(`end buildSitemap ${path}`);
};

export const buildIndexSitemap = async (paths: string[]): Promise<void> => {
	console.log('start buildSitemap index');

	const Sitemap = configureSitemap({
		baseUrl: BASE_URL,
		include: getDynamicPaths('', paths),
		exclude: ['/*'],
		excludeIndex: true,
		pagesConfig: {
			'/*': {
				priority: '0.5',
				changefreq: 'daily',
			},
		},
		isTrailingSlashRequired: false,
		targetDirectory: `${process.cwd()}/public`,
		pagesDirectory: `${process.cwd()}/pages`,
	});

	await Sitemap.generateSitemap();
	console.log('end buildSitemap index');
};

const createDirectory = (path: string) => {
	const fullPath = `${process.cwd()}/public/${path}`;

	try {
		if (!fs.existsSync(fullPath)) {
			fs.mkdirSync(fullPath);
			console.log(`Directory /public/${path}/ is created.`);
		} else {
			console.log('Directory already exists.');
		}
	} catch (err) {
		console.log(err);
	}
};
