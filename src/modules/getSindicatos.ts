/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import fs from 'fs';
import path from 'path';
import HtmlTableToJson from 'html-table-to-json';

import {
	DirectivaType,
	EmpresaType,
	GroupedUnions,
	PaginatedUnions,
	RegionType, UnionsRawType, UnionsType, TableToJsonParserType, UnionCategory,
} from './types';

const DATA_PATH = process.env.NODE_ENV === 'test' ? 'test/mocks/data' : 'data';
const UNIONS_PER_PAGE = parseInt(process.env.UNIONS_PER_PAGE as string, 10);

const getRegions = (): RegionType[] => {
	const sindicatosDataPath = path.join(process.cwd(), DATA_PATH, 'html');
	const filenames = fs.readdirSync(sindicatosDataPath);

	const filteredRegions = filenames.filter((filename) => filename.split('_').length > 2);

	return filteredRegions.map(buildRegion);
};

const getCategories = (): UnionCategory[] => {
	let categoryNames: string[] = [];
	let categories: UnionCategory[];

	const regions: RegionType[] = getRegions();
	// regions = regions.slice(0, 2);

	console.log(regions);

	regions.forEach(({ slug }) => {
		const regionNumber = slug.split('_')[0];
		const unions: UnionsType[] = getSindicatosByRegion(regionNumber);

		categoryNames = unions.map((union) => union.tipo);
		categoryNames = Array.from(new Set(categoryNames));
	});

	console.log(categoryNames);

	categories = categoryNames.map(buildCategory);
	categories = Array.from(new Set(categories));

	return categories;
};

const getSindicatosByRegion = (regionName: string): UnionsType[] => {
	const sindicatosDataPath = path.join(process.cwd(), DATA_PATH, 'html');
	const filenames = fs.readdirSync(sindicatosDataPath);

	let region = filenames.find((file) => {
		const name = file.split('_')[2].split('.')[0];
		return name === regionName;
	});

	region = region || '';

	const filePath = path.join(sindicatosDataPath, region);
	const fileContents = fs.readFileSync(filePath, 'utf8');
	const parsedData: TableToJsonParserType = HtmlTableToJson.parse(fileContents) as TableToJsonParserType;

	const sindicatos: UnionsRawType[] = parsedData.results[0] as UnionsRawType[];

	return parseSindicatos(sindicatos);
};

const getPaginated = (unions: UnionsType[], maxPerPage = UNIONS_PER_PAGE): PaginatedUnions => {
	const unionsCount = unions.length;

	const pagesCount = Math.floor(unionsCount / maxPerPage) + 1;

	const pages: UnionsType[][] = [];

	unions.forEach((union, unionIndex) => {
		const pagesIndex = Math.floor(unionIndex / maxPerPage);

		if (!pages[pagesIndex]) pages[pagesIndex] = [];

		pages[pagesIndex].push(union);
	});

	return {
		unionsCount,
		pagesCount,
		pages,
	};
};

const getPagedUnionsFromDisk = (slug: string): PaginatedUnions | null => {
	const filePath = path.join(process.cwd(), DATA_PATH, `${slug}.json`);

	console.log('path: ', filePath);

	try {
		console.info(`start getPagedUnionsFromDisk: union ${slug}`);
		const fileContent = fs.readFileSync(filePath, 'utf-8');

		const parsedUnions = JSON.parse(fileContent) as PaginatedUnions;

		console.info(`end getPagedUnionsFromDisk: union ${slug}`);

		return parsedUnions;
	} catch (e) {
		console.error(`getPagedUnionsFromDisk: unable to find union ${slug} on disk`);
		return null;
	}
};

const savePagedUnionsToDisk = async (pages: PaginatedUnions, slug: string): Promise<void> => {
	const filePath = path.join(process.cwd(), DATA_PATH, `${slug}.json`);
	try {
		console.info(`start savePagedUnionsToDisk: union ${slug}`);
		await fs.promises.writeFile(filePath, JSON.stringify(pages), 'utf8');
		console.info(`end savePagedUnionsToDisk: union ${slug}`);
	} catch (err) {
		console.error(`savePagedUnionsToDisk: unable to write union ${slug} on disk`);

		throw err;
	}
};

const parseSindicatos = (sindicatos: UnionsRawType[]): UnionsType[] => {
	const groupedData: GroupedUnions = groupSindicatos(sindicatos);
	const groups = Object.keys(groupedData);

	return groups.map((group) => buildSindicato(groupedData[group]));
};

const groupSindicatos = (sindicatos: UnionsRawType[]): GroupedUnions => {
	const accumulative: GroupedUnions = {};

	const grouped = sindicatos.reduce((acc, d) => {
		const hasName = Object.keys(acc).includes(d.nombre);
		const hasCompany = Object.keys(acc).includes(d.Empresa);
		if (hasName && hasCompany) return acc;

		const groupKey = `${d.nombre}-${d.Empresa}`;

		acc[groupKey] = sindicatos.filter((g) => (g.nombre === d.nombre) && (g.Empresa === d.Empresa));

		return acc;
	}, accumulative);

	return grouped;
};

const buildSindicato = (group: UnionsRawType[]): UnionsType => {
	const directiva: DirectivaType[] = getDirectiva(group);

	const email = group[0].Email !== 'NULL' ? group[0].Email : '';

	return {
		nombre: group[0].nombre,
		empresa: getEmpresa(group[0].Empresa),
		tipo: group[0].glosa1,
		direccion: group[0].Direccion,
		region: group[0].Region,
		telefono: group[0].fono,
		fechaConstitucion: group[0].FechaConstitucion,
		fechaDepositoEstatutos: group[0].FechaDepositoEstatutos,
		fechaUltimaDirectiva: group[0].FechaUltimaDirectiva,
		cantidadMiembros: {
			socias: parseInt(group[0].Socias, 10),
			socios: parseInt(group[0].Socios, 10),
		},
		email,
		directiva,
	};
};

const getDirectiva = (group: UnionsRawType[]): DirectivaType[] => group.map((o) => ({
	nombreDirigente: o.NombreDirigente,
	estado: o.glosa,
	rol: o.glosa2,
	finMandato: o.FinMandato,
	inicioMandato: o.InicioMandato,
}));

const getEmpresa = (rawEmpresa: string): EmpresaType => {
	const matchRut = /(\d+)-(\d)/.exec(rawEmpresa);
	const rut = matchRut ? matchRut[0] : '';

	const matchNombre = /([a-zA-Z].+)/.exec(rawEmpresa);
	const nombre = matchNombre ? matchNombre[0] : '';

	return {
		rut,
		nombre,
	};
};

const buildRegion = (filename: string): RegionType => {
	const filenameParts: string[] = filename.replace('.html', '').split('_');

	let regionName: string = filenameParts[2].replace('-', ' ');
	regionName = regionName.split(' ')
		.map((part) => part[0].toUpperCase() + part.slice(1))
		.join(' ');

	const name = `Región de ${regionName}`;
	const slug = filenameParts[2];
	const number = filenameParts[1];

	return {
		name,
		slug,
		number,
	};
};

const buildCategory = (categoryName: string): UnionCategory => ({
	name: categoryName,
	slug: categoryName.replace(/ /g, '-').toLocaleLowerCase(),
});

export {
	getRegions, getSindicatosByRegion, getPaginated, getPagedUnionsFromDisk, savePagedUnionsToDisk,
	getCategories,
};
