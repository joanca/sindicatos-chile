export type TableToJsonParserType = {
	results: Array<unknown>
};

export type RegionType = {
	name: string,
	slug: string,
	number: string
};

export type UnionsRawType = {
	Region: string,
	rsu_raf: string,
	nombre: string,
	glosa: string,
	fono: string,
	glosa1: string,
	Direccion: string,
	Email: string,
	Socios: string,
	Socias: string,
	FechaConstitucion: string,
	Column1: string,
	FechaUltimaDirectiva: string,
	NombreOficina: string,
	FechaDepositoEstatutos: string,
	Empresa: string,
	NombreDirigente: string,
	InicioMandato: string,
	FinMandato: string,
	glosa2: string,
};

export type DirectivaType = {
	nombreDirigente: string,
	estado: string,
	rol: string,
	finMandato: string,
	inicioMandato: string
};

export type EmpresaType = {
	nombre: string,
	rut: string,
};

export type GroupedUnions = {
	[key: string]: UnionsRawType[]
};

export type UnionsType = {
	nombre: string,
	empresa: {
		nombre: string,
		rut: string
	},
	tipo: string,
	direccion: string,
	email: string,
	region: string,
	telefono: string,
	fechaConstitucion: string,
	fechaDepositoEstatutos: string,
	fechaUltimaDirectiva: string,
	cantidadMiembros: {
		socios: number,
		socias: number
	},
	directiva: DirectivaType[]
};

export type PaginatedUnions = {
	unionsCount: number,
	pagesCount: number,
	pages: Array<UnionsType[]>
};

export type UnionCategory = {
	name: string,
	slug: string
};
