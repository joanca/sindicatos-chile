import React, { FunctionComponent } from 'react';
import { v4 } from 'uuid';

import { RegionType, UnionCategory } from 'src/modules/types';

import styles from './Home.module.scss';

export type HomeProps = {
	regions: RegionType[],
	categories: UnionCategory[]
};

const Home: FunctionComponent<HomeProps> = ({ regions, categories }) => (
	<>
		<main className={styles.container}>
			<header>
				<h1>Sindicatos de Chile</h1>
				<h2>Somos el directorio más completo de sindicatos del país, te sorprenderá la cantidad!</h2>
			</header>

			<section>
				<p>Por el momento puedes filtrar por región y por categoría:</p>

				<section className={styles['filters-columns']}>
					<div>
						<h3>Regiones</h3>
						{regions.map((region: RegionType) => (
							<li key={v4()}><a href={`/region/${region.slug}`}>{region.name}</a></li>
						))}
					</div>

					<div>
						<h3>Categorías</h3>
						{categories.map((category: UnionCategory) => (
							<li key={v4()}><a href={`/categoria/${category.slug}`}>{category.name}</a></li>
						))}
					</div>

				</section>
			</section>

		</main>
	</>
);

export { Home };
