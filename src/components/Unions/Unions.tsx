import React, { FunctionComponent } from 'react';
import { useRouter } from 'next/router';

import { UnionsType } from 'src/modules/types';
import { UnionsList } from './UnionsList';
import { Pagination } from './Pagination';

import styles from './Unions.module.scss';

export type UnionsProps = {
	unions: UnionsType[],
	pageIndex: number,
	slug: string,
	pagesCount: number
};

const Unions: FunctionComponent<UnionsProps> = (props) => {
	const {
		unions = [], pageIndex, slug, pagesCount,
	} = props;

	const router = useRouter();

	if (router.isFallback) {
		return <div>Loading...</div>;
	}

	return (
		<main className={styles['page-container']}>
			<UnionsList sindicatos={unions} />

			<Pagination
				currentPage={pageIndex}
				totalPages={pagesCount}
				slug={slug}
			/>
		</main>
	);
};

export { Unions };
