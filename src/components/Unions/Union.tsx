import React, { FunctionComponent } from 'react';
import { UnionsType } from 'src/modules/types';

const Union: FunctionComponent<UnionsType> = (union) => {
	const members = union.cantidadMiembros.socias + union.cantidadMiembros.socios;

	return (
		<article>
			<h3>{union.nombre}</h3>
			<li><strong>Empresa:</strong> {union.empresa.nombre}</li>
			<li>{getEmail(union.email)}</li>
			<li><strong>Miembros:</strong> {members}</li>
			<li>{getCategory(union.tipo)}</li>
		</article>
	);
};

const getEmail = (email: string) => {
	const emailAnchor = <a href={`mailto: ${email.toLocaleLowerCase()}`}>{email.toLocaleLowerCase()}</a>;
	return (
		<span><strong>Email:</strong> {emailAnchor}</span>
	);
};

const getCategory = (category: string) => {
	const categorySlug = category.replace(/ /g, '-').toLocaleLowerCase();
	const categoryAnchor = <a href={`/categoria/${categorySlug}`}>{category}</a>;

	return (
		<span><strong>Categoría: </strong> {categoryAnchor}</span>
	);
};

export { Union };
