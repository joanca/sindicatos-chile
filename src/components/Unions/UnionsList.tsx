import React, { FunctionComponent } from 'react';
import { v4 } from 'uuid';

import { Union } from 'src/components/Unions/Union';
import { UnionsType } from 'src/modules/types';

import styles from './Unions.module.scss';

export type UnionsListProps = {
	sindicatos: UnionsType[]
};

const UnionsList: FunctionComponent<UnionsListProps> = ({ sindicatos }) => (
	<main className={styles['union-list-container']}>
		{sindicatos.map((union) => <Union key={v4()} {...union} />)}
	</main>
);

export { UnionsList };
