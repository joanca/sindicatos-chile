import React, { FunctionComponent, SyntheticEvent } from 'react';
import { useRouter } from 'next/router';

import {
	Pagination as PaginationSUI,
	PaginationProps as PaginationPropsSUI,
} from 'semantic-ui-react';

type PaginationProps = {
	currentPage: number,
	totalPages: number,
	slug: string
};

const Pagination: FunctionComponent<PaginationProps> = ({ currentPage, totalPages, slug }) => {
	const router = useRouter();

	const handlePageChange = async (_: SyntheticEvent, data: PaginationPropsSUI) => {
		const nextPage = data.activePage as number === 1 ? '' : data.activePage as number;
		const href = `${slug}/${nextPage}`;

		await router.push(href);
	};

	return (
		<section className="pagination">
			<PaginationSUI
				totalPages={totalPages}
				activePage={currentPage}
				onPageChange={handlePageChange}
				pointing
				secondary
			/>
		</section>
	);
};

export { Pagination };
