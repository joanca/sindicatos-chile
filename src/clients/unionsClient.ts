import firestore, { ServiceAccount } from 'firebase-admin';
import { v4 } from 'uuid';

import { RegionType, UnionsType, UnionCategory } from 'src/modules/types';
import { getCredentials } from 'config/firestore';

const UNIONS_COLLECTION = 'unions';
const REGIONS_COLLECTION = 'regions';
const CATEGORIES_COLLECTION = 'categories';

export class UnionsClient {
	private db: FirebaseFirestore.Firestore;

	constructor() {
		if (!firestore.apps.length) {
			const serviceAccount = getCredentials();

			firestore.initializeApp({
				credential: firestore.credential.cert(serviceAccount as ServiceAccount),
				databaseURL: 'https://sindicatos-30cba.firebaseio.com',
			});
		}

		this.db = firestore.firestore();
	}

	async addUnion(union: UnionsType): Promise<void> {
		const unionRef = this.db.collection(UNIONS_COLLECTION).doc(v4());

		await unionRef.set(union);
	}

	async addRegion(region: RegionType): Promise<void> {
		const regionRef = this.db.collection(REGIONS_COLLECTION).doc(v4());

		await regionRef.set(region);
	}

	async addCategory(category: UnionCategory): Promise<void> {
		const categoryRef = this.db.collection(CATEGORIES_COLLECTION).doc(v4());

		await categoryRef.set(category);
	}

	async getUnionList(): Promise<UnionsType[]> {
		const unions: UnionsType[] = [];

		const querySnapshot = await this.db.collection(UNIONS_COLLECTION).get();
		querySnapshot.forEach((doc) => unions.push(doc.data() as UnionsType));

		return unions;
	}

	async getUnionsByRegion(region: RegionType): Promise<UnionsType[]> {
		const unions: UnionsType[] = [];

		const querySnapshot = this.db.collection(UNIONS_COLLECTION)
			.where('region', '==', region.number)
			.orderBy('nombre');

		const documents = await querySnapshot.get();

		documents.forEach((doc) => unions.push(doc.data() as UnionsType));

		return unions;
	}

	async getRegions(): Promise<RegionType[]> {
		const regions: RegionType[] = [];

		const querySnapshot = await this.db.collection(REGIONS_COLLECTION).get();
		querySnapshot.forEach((doc) => regions.push(doc.data() as RegionType));

		return regions;
	}

	async getUnionsCategories(): Promise<UnionCategory[]> {
		const types: UnionCategory[] = [];

		const querySnapshot = await this.db.collection(CATEGORIES_COLLECTION).get();
		querySnapshot.forEach((doc) => types.push(doc.data() as UnionCategory));

		return types;
	}

	async getUnionsByCategory(category: UnionCategory): Promise<UnionsType[]> {
		const unions: UnionsType[] = [];

		const querySnapshot = this.db.collection(UNIONS_COLLECTION)
			.where('tipo', '==', category.name);

		const documents = await querySnapshot.get();

		documents.forEach((doc) => unions.push(doc.data() as UnionsType));

		return unions;
	}
}
