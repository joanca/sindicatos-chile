import { RegionType, UnionsType, UnionCategory } from 'src/modules/types';

export interface UnionsClientInterface {
	addUnion(union: UnionsType): Promise<void>;
	addRegion(region: RegionType): Promise<void>,
	addCategory(category: UnionCategory): Promise<void>,
	getRegions(): Promise<RegionType[]>,
	getUnionList(): Promise<UnionsType[]>,
	getUnionsByCategory(category: UnionCategory): Promise<UnionsType[]>,
	getUnionsByRegion(region: RegionType): Promise<UnionsType[]>,
	getUnionsCategories(): Promise<UnionCategory[]>,
}
