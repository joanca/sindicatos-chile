import firebase from 'firebase-admin';

import { getStaticPaths, getStaticProps } from 'pages/region/[slug]';
import { RegionType, UnionsType } from 'src/modules/types';
import { FirestoreMock, collectionGetMockBuilder } from 'test/mocks/db/firestore';

jest.mock('firebase-admin', () => ({
	apps: [{}],
	firestore: jest.fn(),
}));

jest.spyOn(firebase, 'firestore').mockImplementation(() => FirestoreMock);

const region1: RegionType = {
	name: 'region 1',
	slug: 'region-1',
	number: '1',
};
const region2: RegionType = {
	name: 'region 2',
	slug: 'region-2',
	number: '2',
};

describe('region/ [slug]', () => {
	describe('getStaticPaths', () => {
		beforeEach(() => {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-call
			collectionGetMockBuilder([region1, region2]);
		});

		it("should return static paths with region's slug", async () => {
			const expectedPaths = {
				fallback: false,
				paths: [{ params: { slug: 'region-1' } }, { params: { slug: 'region-2' } }],
			};

			expect(await getStaticPaths()).toEqual(expectedPaths);
		});
	});

	describe('getStaticProps', () => {
		const mockContext = {
			params: {
				slug: 'region-2',
			},
		};

		beforeEach(() => {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-call
			collectionGetMockBuilder([region1, region2]);
			collectionGetMockBuilder([sindicato1, sindicato2, sindicato3]);
		});

		it('should return props with unions by region', async () => {
			const expectedProps = {
				props: {
					pageIndex: 1,
					pageTitle: 'Sindicatos de region 2',
					pagesCount: null,
					slug: '/region/region-2',
					unions: undefined,
				},
			};

			expect(await getStaticProps(mockContext)).toEqual(expectedProps);
		});
	});
});

const sindicato1: UnionsType = {
	nombre: 'SINDICATO DE EMPRESA ACF NITRATO S.A.',
	empresa: {
		nombre: 'ACF NITRATOS S.A.',
		rut: '99572020-5',
	},
	direccion: 'SERRANO Nº498',
	tipo: 'SINDICATO EMPRESA',
	email: '',
	region: '01',
	telefono: '',
	fechaConstitucion: '24/02/2005',
	fechaDepositoEstatutos: '09/03/2005',
	fechaUltimaDirectiva: '05-04-2016 0:00:00',
	cantidadMiembros: {
		socias: 0,
		socios: 64,
	},
	directiva: [
		{
			nombreDirigente: 'WALTER JAIME BARRAZA DIAZ',
			estado: 'ACTIVO',
			rol: 'PRESIDENTE',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
		{
			nombreDirigente: 'SERGIO REYES ULLOA',
			estado: 'ACTIVO',
			rol: 'SECRETARIO',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
		{
			nombreDirigente: 'JOSE LUIS ARAYA MANCILLA',
			estado: 'ACTIVO',
			rol: 'TESORERO',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
	],
};

const sindicato2: UnionsType = {
	cantidadMiembros: {
		socias: 0,
		socios: 168,
	},
	direccion: "'EL MORRO BLOCK - B-1 DPTO. 103'",
	directiva: [
		{
			estado: 'ACTIVO',
			finMandato: '13/09/2020',
			inicioMandato: '27/05/2016',
			nombreDirigente: 'CARLOS FRANCISCO CHAVARINI VEGA',
			rol: 'PRESIDENTE',
		},
	],
	email: '',
	empresa: { nombre: '', rut: '' },
	tipo: 'FEDERACION DE SINDICATOS',
	fechaConstitucion: '27/05/2016',
	fechaDepositoEstatutos: '02/06/2016',
	fechaUltimaDirectiva: '01-01-1980 0:00:00',
	nombre: 'FEDERACION NACIONAL PATRONES Y MOTORISTAS "FENAPAM"',
	region: '01',
	telefono: '57-2321129',
};

const sindicato3: UnionsType = {
	cantidadMiembros: {
		socias: 0,
		socios: 168,
	},
	direccion: 'DIRECCION',
	directiva: [
		{
			estado: 'ACTIVO',
			finMandato: '13/09/2020',
			inicioMandato: '27/05/2016',
			nombreDirigente: 'CARLOS FRANCISCO CHAVARINI VEGA',
			rol: 'PRESIDENTE',
		},
	],
	email: '',
	empresa: { nombre: 'EMPRESA', rut: '123-0' },
	tipo: 'FEDERACION DE SINDICATOS',
	fechaConstitucion: '27/05/2016',
	fechaDepositoEstatutos: '02/06/2016',
	fechaUltimaDirectiva: '01-01-1980 0:00:00',
	nombre: 'SINDICATO',
	region: '01',
	telefono: '57-2321129',
};
