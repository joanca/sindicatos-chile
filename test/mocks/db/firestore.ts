import { CollectionReference, DocumentData } from '@google-cloud/firestore';

const getColletion = jest.fn();
export const collection: Partial<CollectionReference<DocumentData>> = {
	get: getColletion,
	where: jest.fn().mockReturnValue({
		orderBy: jest.fn().mockReturnValue({
			get: getColletion,
		}),
	}),
};

export const FirestoreMock: FirebaseFirestore.Firestore = {
	collection: jest.fn().mockReturnValue(collection),
	settings: jest.fn(),
	doc: jest.fn(),
	collectionGroup: jest.fn(),
	getAll: jest.fn(),
	terminate: jest.fn(),
	listCollections: jest.fn(),
	runTransaction: jest.fn(),
	batch: jest.fn(),
	bulkWriter: jest.fn(),
};

export const collectionGetMockBuilder = (values: unknown[]): void => {
	const mock = values.map((val) => ({
		data: () => val,
	}));

	getColletion.mockResolvedValueOnce(mock);
};
