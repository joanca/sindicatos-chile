const originalConsoleLog = console.log;
const originalConsoleInfo = console.info;
const originalConsoleError = console.error;

console.log = (message) => {
	if(/path: /i.test(message)) return ;

	originalConsoleLog(message)
}

console.info = (message) => {
	// remove logs from tests
	if(/start /i.test(message)) return ;
	if(/end /i.test(message)) return ;

	originalConsoleInfo(message)
}