import { getPaginated, getRegions, getSindicatosByRegion } from 'src/modules/getSindicatos';
import { PaginatedUnions, RegionType, UnionsType } from 'src/modules/types';

describe('getSindicatos', () => {
	it('should return path of sindicatos by region', () => {
		const expectedRegions: RegionType[] = [{
			name: 'Región de Region Name',
			slug: 'region-name',
			number: '01',
		}];

		expect(getRegions()).toEqual(expectedRegions);
	});

	it('should return sindicatos of one region', () => {
		const expectedSindicatos: UnionsType[] = [sindicato1, sindicato2, sindicato3];

		expect(getSindicatosByRegion('region-name')).toEqual(expectedSindicatos);
	});

	it('should return paginated unions', () => {
		const unions = getSindicatosByRegion('region-name');

		const expectedPages: PaginatedUnions = {
			pagesCount: 2,
			unionsCount: 3,
			pages: [[sindicato1, sindicato2], [sindicato3]],
		};

		expect(getPaginated(unions, 2)).toEqual(expectedPages);
	});
});

const sindicato1: UnionsType = {
	nombre: 'SINDICATO DE EMPRESA ACF NITRATO S.A.',
	empresa: {
		nombre: 'ACF NITRATOS S.A.',
		rut: '99572020-5',
	},
	direccion: 'SERRANO Nº498',
	tipo: 'SINDICATO EMPRESA',
	email: '',
	region: '01',
	telefono: '',
	fechaConstitucion: '24/02/2005',
	fechaDepositoEstatutos: '09/03/2005',
	fechaUltimaDirectiva: '05-04-2016 0:00:00',
	cantidadMiembros: {
		socias: 0,
		socios: 64,
	},
	directiva: [
		{
			nombreDirigente: 'WALTER JAIME BARRAZA DIAZ',
			estado: 'ACTIVO',
			rol: 'PRESIDENTE',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
		{
			nombreDirigente: 'SERGIO REYES ULLOA',
			estado: 'ACTIVO',
			rol: 'SECRETARIO',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
		{
			nombreDirigente: 'JOSE LUIS ARAYA MANCILLA',
			estado: 'ACTIVO',
			rol: 'TESORERO',
			finMandato: '13/09/2020',
			inicioMandato: '29/04/2016',
		},
	],
};

const sindicato2: UnionsType = {
	cantidadMiembros: {
		socias: 0,
		socios: 168,
	},
	direccion: "'EL MORRO BLOCK - B-1 DPTO. 103'",
	directiva: [
		{
			estado: 'ACTIVO',
			finMandato: '13/09/2020',
			inicioMandato: '27/05/2016',
			nombreDirigente: 'CARLOS FRANCISCO CHAVARINI VEGA',
			rol: 'PRESIDENTE',
		},
	],
	email: '',
	empresa: { nombre: '', rut: '' },
	tipo: 'FEDERACION DE SINDICATOS',
	fechaConstitucion: '27/05/2016',
	fechaDepositoEstatutos: '02/06/2016',
	fechaUltimaDirectiva: '01-01-1980 0:00:00',
	nombre: 'FEDERACION NACIONAL PATRONES Y MOTORISTAS "FENAPAM"',
	region: '01',
	telefono: '57-2321129',
};

const sindicato3: UnionsType = {
	cantidadMiembros: {
		socias: 0,
		socios: 168,
	},
	direccion: 'DIRECCION',
	directiva: [
		{
			estado: 'ACTIVO',
			finMandato: '13/09/2020',
			inicioMandato: '27/05/2016',
			nombreDirigente: 'CARLOS FRANCISCO CHAVARINI VEGA',
			rol: 'PRESIDENTE',
		},
	],
	email: '',
	empresa: { nombre: 'EMPRESA', rut: '123-0' },
	tipo: 'FEDERACION DE SINDICATOS',
	fechaConstitucion: '27/05/2016',
	fechaDepositoEstatutos: '02/06/2016',
	fechaUltimaDirectiva: '01-01-1980 0:00:00',
	nombre: 'SINDICATO',
	region: '01',
	telefono: '57-2321129',
};
