/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable global-require */
type CREDENTIALS = {
	type: string,
	project_id: string,
	private_key_id: string,
	private_key: string,
	client_email: string,
	client_id: string,
	auth_uri: string,
	token_uri: string,
	auth_provider_x509_cert_url: string,
	client_x509_cert_url: string,
};

const getCredentials = (): CREDENTIALS => {
	const encodedCredentials = process.env.GCLOUD_CREDENTIALS || '';
	const credentialsString = Buffer.from(encodedCredentials, 'base64').toString('ascii');

	const parseCredentials = JSON.parse(credentialsString) as CREDENTIALS;

	return parseCredentials;
};

export { getCredentials };
