module.exports = {
  preset: 'ts-jest',
  clearMocks: true,
  setupFilesAfterEnv: ['<rootDir>/test/__setup__/setupTests.js'],
  testEnvironment: 'jsdom',
  testMatch: ['<rootDir>/**/*.spec.ts?(x)'],
  moduleDirectories: ["node_modules", '.'],
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/test/__setup__/fileMock.js",
    "\\.(css|less|scss)$": "<rootDir>/test/__setup__/styleMock.js"
  },
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.test.json'
    }
  }
};