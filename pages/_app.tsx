import { NextComponentType } from 'next';
import { AppContext, AppInitialProps, AppProps } from 'next/app';
import Head from 'next/head';

import 'src/components/styles.scss';

type AppPropsWithTitle = {
	[key: string]: unknown,
	pageTitle: string,
};

const GOOGLE_ANALYTICS = process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS as string;

const App: NextComponentType<AppContext, AppInitialProps, AppProps> = ({ Component, pageProps }) => {
	const { pageTitle } = pageProps as AppPropsWithTitle;

	return (
		<>
			<Head>
				<title>{pageTitle}</title>
				<link
					href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
					rel="stylesheet"
				/>
				<script
					async
					src={`https://www.googletagmanager.com/gtag/js?id=${GOOGLE_ANALYTICS}`}
				/>
			</Head>
			<Component {...pageProps} />

			<script
				// eslint-disable-next-line react/no-danger
				dangerouslySetInnerHTML={{
					__html: `
						window.dataLayer = window.dataLayer || [];
						function gtag(){dataLayer.push(arguments);}
						gtag('js', new Date());
						gtag('config', '${GOOGLE_ANALYTICS}');
					`,
				}}
			/>

		</>
	);
};

export default App;
