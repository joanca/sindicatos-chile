import { GetStaticPaths, GetStaticProps } from 'next';

import { UnionsProps, Unions } from 'src/components/Unions/Unions';
import {
	getPaginated, getPagedUnionsFromDisk, savePagedUnionsToDisk,
} from 'src/modules/getSindicatos';

import {
	UnionsType, UnionCategory,
} from 'src/modules/types';

import { UnionsClient } from 'src/clients/unionsClient';

type Pages = {
	slug: string,
	index: number
};

type StaticPaths = {
	params: string[]
};

const unionsClient = new UnionsClient();

export const getStaticPaths: GetStaticPaths<StaticPaths> = async () => {
	const categories: UnionCategory[] = await unionsClient.getUnionsCategories();
	const pages: Pages[] = [];

	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	categories.forEach(async (category) => {
		let paginated = getPagedUnionsFromDisk(category.slug);

		if (!paginated) {
			const unions: UnionsType[] = await unionsClient.getUnionsByCategory(category);
			paginated = getPaginated(unions);

			await savePagedUnionsToDisk(paginated, category.slug);
		}

		paginated.pages.forEach((_, index) => pages.push({ slug: category.slug, index: index + 1 }));
	});

	const paths = pages.map(({ slug, index }) => ({
		params: { params: [slug, String(index)] },
	}));

	return {
		paths,
		fallback: true,
	};
};

export const getStaticProps: GetStaticProps<UnionsProps> = async ({ params }) => {
	const [slug, pageIndex] = (params as StaticPaths).params;

	const index = parseInt(pageIndex, 10) - 1;

	const categories: UnionCategory[] = await unionsClient.getUnionsCategories();
	const category = categories.find((c) => c.slug === slug) as UnionCategory;

	let paginated = getPagedUnionsFromDisk(slug);

	if (!paginated) {
		const unions: UnionsType[] = await unionsClient.getUnionsByCategory(category);
		paginated = getPaginated(unions);

		await savePagedUnionsToDisk(paginated, slug);
	}
	return {
		props: {
			unions: paginated.pages[index],
			pageIndex: index + 1,
			pagesCount: paginated.pagesCount,
			slug: `/categoria/${slug}`,
			pageTitle: category.name,
		},
	};
};

export default Unions;
