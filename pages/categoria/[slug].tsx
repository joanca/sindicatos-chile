import { GetStaticProps, GetStaticPaths } from 'next';

import {
	getPagedUnionsFromDisk, getPaginated, savePagedUnionsToDisk,
} from 'src/modules/getSindicatos';

import { UnionsType, UnionCategory } from 'src/modules/types';
import { Unions, UnionsProps } from 'src/components/Unions/Unions';
import { UnionsClient } from 'src/clients/unionsClient';
import { buildSitemap } from 'src/modules/generateSitemap';

type StaticPaths = {
	slug: string
};

const unionsClient = new UnionsClient();

export const getStaticPaths: GetStaticPaths<StaticPaths> = async () => {
	const categories: UnionCategory[] = await unionsClient.getUnionsCategories();

	const sitemapPaths = categories.map((category) => category.slug);

	await buildSitemap('categoria', sitemapPaths);

	const paths = categories.map((type) => ({
		params: { slug: type.slug },
	}));

	return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<UnionsProps> = async ({ params }) => {
	const { slug } = params as StaticPaths;

	const categories: UnionCategory[] = await unionsClient.getUnionsCategories();
	const category = categories.find((c) => c.slug === slug) as UnionCategory;

	let paginated = getPagedUnionsFromDisk(slug);

	if (!paginated) {
		const unions: UnionsType[] = await unionsClient.getUnionsByCategory(category);
		paginated = getPaginated(unions);

		await savePagedUnionsToDisk(paginated, slug);
	}

	return {
		props: {
			unions: paginated.pages[0],
			pageIndex: 1,
			pagesCount: paginated.pagesCount,
			slug: `/categoria/${slug}`,
			pageTitle: category.name,
		},
	};
};

export default Unions;
