import { GetStaticPaths, GetStaticProps } from 'next';

import { UnionsProps, Unions } from 'src/components/Unions/Unions';
import {
	getPaginated, getPagedUnionsFromDisk, savePagedUnionsToDisk,
} from 'src/modules/getSindicatos';

import {
	RegionType, UnionsType,
} from 'src/modules/types';

import { UnionsClient } from 'src/clients/unionsClient';

type Pages = {
	slug: string,
	index: number
};

type StaticPaths = {
	params: string[]
};

const unionsClient = new UnionsClient();

export const getStaticPaths: GetStaticPaths<StaticPaths> = async () => {
	const regions: RegionType[] = await unionsClient.getRegions();
	const pages: Pages[] = [];

	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	regions.forEach(async (region) => {
		let paginated = getPagedUnionsFromDisk(region.slug);

		if (!paginated) {
			const unions: UnionsType[] = await unionsClient.getUnionsByRegion(region);
			paginated = getPaginated(unions);

			await savePagedUnionsToDisk(paginated, region.slug);
		}

		paginated.pages.forEach((_, index) => pages.push({ slug: region.slug, index: index + 1 }));
	});

	const paths = pages.map(({ slug, index }) => ({
		params: { params: [slug, String(index)] },
	}));

	return {
		paths,
		fallback: true,
	};
};

export const getStaticProps: GetStaticProps<UnionsProps> = async ({ params }) => {
	const [slug, pageIndex] = (params as StaticPaths).params;

	const index = parseInt(pageIndex, 10) - 1;

	const regions: RegionType[] = await unionsClient.getRegions();
	const region = regions.find((r) => r.slug === slug) as RegionType;

	let paginated = getPagedUnionsFromDisk(slug);

	if (!paginated) {
		const unions: UnionsType[] = await unionsClient.getUnionsByRegion(region);
		paginated = getPaginated(unions);

		await savePagedUnionsToDisk(paginated, slug);
	}
	return {
		props: {
			unions: paginated.pages[index],
			pageIndex: index + 1,
			pagesCount: paginated.pagesCount,
			slug: `/region/${slug}`,
			pageTitle: `Sindicatos de ${region.name}`,
		},
	};
};

export default Unions;
