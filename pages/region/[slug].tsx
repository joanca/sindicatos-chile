import { GetStaticProps, GetStaticPaths } from 'next';
import {
	getPagedUnionsFromDisk,
	getPaginated,
	savePagedUnionsToDisk,
} from 'src/modules/getSindicatos';

import { RegionType, UnionsType } from 'src/modules/types';
import { Unions, UnionsProps } from 'src/components/Unions/Unions';
import { UnionsClient } from 'src/clients/unionsClient';
import { buildSitemap } from 'src/modules/generateSitemap';

type StaticPaths = {
	slug: string
};

export const getStaticPaths: GetStaticPaths<StaticPaths> = async () => {
	const unionsClient = new UnionsClient();

	const regions: RegionType[] = await unionsClient.getRegions();

	const sitemapPaths = regions.map((region) => region.slug);

	await buildSitemap('region', sitemapPaths);

	const paths = regions.map((region) => ({
		params: { slug: String(region.slug) },
	}));

	return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<UnionsProps> = async ({ params }) => {
	const unionsClient = new UnionsClient();

	const { slug } = params as StaticPaths;

	const regions: RegionType[] = await unionsClient.getRegions();
	const region = regions.find((r) => r.slug === slug) as RegionType;

	let paginated = getPagedUnionsFromDisk(slug);

	if (!paginated) {
		const unions: UnionsType[] = await unionsClient.getUnionsByRegion(region);
		paginated = getPaginated(unions);

		await savePagedUnionsToDisk(paginated, slug);
	}

	return {
		props: {
			unions: paginated.pages[0],
			pageIndex: 1,
			pagesCount: paginated.pagesCount,
			slug: `/region/${slug}`,
			pageTitle: `Sindicatos de ${region.name}`,
		},
	};
};

export default Unions;
