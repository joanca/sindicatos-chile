import { GetStaticProps } from 'next';
import { UnionsClient } from 'src/clients/unionsClient';

import { Home, HomeProps } from 'src/components/Home/Home';
import { buildIndexSitemap } from 'src/modules/generateSitemap';
import { RegionType, UnionCategory } from 'src/modules/types';

const unionsClient = new UnionsClient();

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
	const regions: RegionType[] = await unionsClient.getRegions();
	const categories: UnionCategory[] = await unionsClient.getUnionsCategories();

	const sitemapPaths = ['region/sitemap.xml', 'categoria/sitemap.xml'];

	await buildIndexSitemap(sitemapPaths);

	return {
		props: {
			regions,
			categories,
			pageTitle: 'Sindicatos de Chile',
		},
	};
};

export default Home;
